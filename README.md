# Online Car Parking Garage Portal
  * [Problem Statement](./docs/pstatement.md)
  * [Use Case Diagram](./docs/usecase.md) 

# UML
  * [Quick-reference](http://www.mcdonaldland.info/files/designpatterns/designpatternscard.pdf)
  * [Cheat-Sheet](https://www.slideshare.net/hustwj/design-patterns-cheat-sheet)

  
# License
  * Except as otherwise noted, the content of this project is licensed under a [MIT License](./LICENSE)

**License Compatibility**  
  * [Choose a License](https://choosealicense.com/licenses/)
  * [GPL compatible Licenses](https://www.gnu.org/licenses/license-list.html#GPLCompatibleLicenses)
  * [https://en.wikipedia.org/wiki/License_compatibility](https://en.wikipedia.org/wiki/License_compatibility)  
  * [https://www.dwheeler.com/essays/floss-license-slide.html](https://www.dwheeler.com/essays/floss-license-slide.html)  
  * [https://janelia-flyem.github.io/licenses.html](https://janelia-flyem.github.io/licenses.html)  
    
  

