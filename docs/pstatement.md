# Online Car Parking Garage Portal
  ### Possible functionalities that may be covered:

* Registration of different categories of users.
* Showing information regarding garage.
* Query regarding car parking and service.
* Payment of bills.
* Identification of registered car against the plate number.
* Cancellation option.